import { asyncData, errorAsyncData } from './_util';

import DashboardApiData from './fixtures/DashboardApiData.json';

export default class DashboardApi {
  static get(status: 200) {
    return asyncData({
      status: status,
      statusText: '',
      headers: DashboardApiData.get[status].headers,
      data: DashboardApiData.get[status].body,

      config: {},
    });
  }
  static get_error(status: 500) {
    return errorAsyncData({
      response: {
        status: status,
        headers: DashboardApiData.get[status].headers,
        data: DashboardApiData.get[status].body,
      },
    });
  }
  static getById(status: 200) {
    return asyncData({
      status: status,
      statusText: '',
      headers: DashboardApiData.getById[status].headers,
      data: DashboardApiData.getById[status].body,

      config: {},
    });
  }
}
