export const asyncData = <T>(data: T, delay: number = 0, callback: () => void = () => {}) =>
  new Promise<T>((res) => {
    setTimeout(() => {
      callback();
      res(data);
    }, delay);
  });
export const errorAsyncData = <T>(data: T, delay: number = 0, callback: () => void = () => {}) =>
  new Promise<never>((_, rej) => {
    setTimeout(() => {
      callback();
      rej(data);
    }, delay);
  });
