import { useCallback, useEffect, useState } from 'react';

import { Card, Container, Grid, Typography } from '@mui/material';

import DashboardApi, { DashboardSchema } from '@/services/DashboardApi';

export default function index() {
  const [loading, setLoading] = useState<boolean>(true);
  const [dashData, setDashData] = useState<DashboardSchema[]>([]);

  useEffect(() => {
    DashboardApi.get()
      .then((res) => {
        setDashData(res.data.items);
      })
      .catch(() => {
        console.log('error');
      })
      .finally(() => {
        setLoading(false);
      });
  }, []);

  const [someCondition, setSomeCondition] = useState<boolean>(false);

  useEffect(() => {
    setTimeout(() => {
      setSomeCondition(true);
    }, 3000);
  }, []);

  const renderSomething = useCallback((data: DashboardSchema[], condition: boolean) => {
    if (data.length > 0 && condition) {
      return data?.map(({ data, name }, i) => {
        return (
          <Grid key={i} item data-testid={`item-${i}`}>
            <Card sx={{ p: 3 }}>
              <Grid container direction="row" alignItems="center" justifyContent="center">
                <Grid item>
                  <Typography variant="h3">{data}</Typography>
                  <Typography variant="caption">{name}</Typography>
                </Grid>
              </Grid>
            </Card>
          </Grid>
        );
      });
    } else {
      return null;
    }
  }, []);

  return (
    <Container>
      {loading ? (
        <Grid container alignItems="center" justifyContent="center" data-testid="loading-indicator">
          <Grid item>Loading...</Grid>
        </Grid>
      ) : (
        <Grid container direction="row" spacing={3} py={3} justifyContent="space-evenly" data-testid="dashboard-container">
          {renderSomething(dashData, someCondition)}
        </Grid>
      )}
    </Container>
  );
}
