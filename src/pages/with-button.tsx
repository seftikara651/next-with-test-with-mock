import { useCallback, useEffect, useState } from 'react';

import { Button, Container, Typography } from '@mui/material';

import { API } from '@/services/WhateverApi';

export default function WithButton() {
  const [data, setData] = useState<string>('no data');
  const [counter, setCounter] = useState<number>(0);

  useEffect(() => {
    setData(() => {
      switch (counter) {
        case 1:
          return 'dog';
        case 2:
          return 'cat';
        case 3:
          return 'hamster';
        default:
          return 'no data';
      }
    });
  }, [counter]);

  const handleClick = useCallback(() => {
    setCounter((prev) => (prev < 3 ? prev + 1 : 0));
  }, []);

  const [customData, setCustomData] = useState<string>('no data');

  useEffect(() => {
    if (counter === 0) {
      API('custom data')
        .then((res) => {
          setCustomData(res);
        })
        .catch(() => {
          setCustomData('error');
        });
    } else {
      API('another data')
        .then((res) => {
          setCustomData(res);
        })
        .catch(() => {
          setCustomData('error');
        });
    }
  }, [counter]);

  return (
    <Container>
      <Button onClick={handleClick} data-testid="trigger-button">
        Click me!
      </Button>
      <Typography variant="h3" data-testid="data-display">
        {data}
      </Typography>
      <Typography variant="h5" data-testid="data-display-2">
        {customData}
      </Typography>
    </Container>
  );
}
