import '@/styles/global.css';

import type { AppProps } from 'next/app';

import { ThemeProvider, createTheme } from '@mui/material';

const THEME = createTheme({});

export default function MyApp({ Component, pageProps }: AppProps) {
  return (
    <ThemeProvider theme={THEME}>
      <Component {...pageProps} />
    </ThemeProvider>
  );
}
