export const API = (data: any) => {
  return new Promise<any>((res) => {
    setTimeout(() => {
      res(data);
    }, 2000);
  });
};
