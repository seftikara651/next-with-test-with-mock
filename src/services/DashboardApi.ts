import axios, { CollectionResponseBody, ResourceResponseBody } from './_axios';

export type DashboardSchema = {
  id: number;
  data: number;
  name: string;
};

export default class DashboardApi {
  static readonly GET_EP = '/dashboard-data';

  static get() {
    return axios.get<CollectionResponseBody<DashboardSchema>>(DashboardApi.GET_EP);
  }
  static getById(id: number) {
    return axios.get<ResourceResponseBody<DashboardSchema>>(`${DashboardApi.GET_EP}/${id}`);
  }
}
