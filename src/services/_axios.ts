import Axios from 'axios';

const axiosClient = Axios.create({
  baseURL: process.env.NEXT_PUBLIC_API_BASE_URL,
});

export default axiosClient;

export type CollectionResponseBody<T> = {
  items: T[];
  count: number;
};
export type ResourceResponseBody<T> = {
  item: T;
};
