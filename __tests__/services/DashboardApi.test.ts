import {} from '@testing-library/react';

import DashboardAPI from '@/services/DashboardApi';

describe('Dashboard API', () => {
  it('should have get all data endpoint', () => {
    expect(DashboardAPI.get).not.toBeNull();
  });
  it('should have get by ID endpoint', () => {
    expect(DashboardAPI.getById).not.toBeNull();
  });

  describe('get all data endpoint', () => {
    it('should be callable', async () => {
      expect(DashboardAPI.get.call).not.toBeNull();
      const r = await DashboardAPI.get();
      expect(r).not.toBeNull();
    });
  });

  describe('get by ID endpoint', () => {
    it('should be callable', async () => {
      expect(DashboardAPI.get.call).not.toBeNull();
      const r = await DashboardAPI.getById(1);
      expect(r).not.toBeNull();
    });
  });
});
