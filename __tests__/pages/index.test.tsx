import { act, render, screen, waitFor } from '@testing-library/react';

import Index from '@/pages/index';

import DashboardApi from '@/services/DashboardApi';
import MockDashboardApi from '__mocks__/DashboardApi';

jest.setTimeout(60000);

describe('Index', () => {
  let dashboard_get: any;

  it('renders without error', () => {
    render(<Index />);
  });

  it('renders dashboard elements on success', async () => {
    dashboard_get = jest.spyOn(DashboardApi, 'get').mockImplementation(() => MockDashboardApi.get(200));

    render(<Index />);

    await waitFor(() => expect(screen.queryByTestId('loading-indicator')).toBeTruthy(), {
      interval: 10,
      timeout: 1000,
    });
    expect(screen.getByTestId('loading-indicator').textContent).toEqual('Loading...');

    await waitFor(() => expect(dashboard_get).toHaveBeenCalled());

    await waitFor(() => expect(screen.queryByTestId('loading-indicator')).not.toBeTruthy(), {
      interval: 10,
      timeout: 1000,
    });
    await waitFor(() => expect(screen.getByTestId('dashboard-container')).toBeVisible());
    await waitFor(() => expect(screen.getByTestId('dashboard-container').childElementCount).toEqual(2), {
      interval: 10,
      timeout: 5000,
    });

    await waitFor(() => expect(screen.queryByTestId('item-0')).toBeTruthy(), {
      interval: 10,
      timeout: 10000,
    });
  });

  it('should handle request error', async () => {
    dashboard_get = jest.spyOn(DashboardApi, 'get').mockImplementation(() => MockDashboardApi.get_error(500));

    render(<Index />);

    await waitFor(() => expect(screen.queryByTestId('loading-indicator')).toBeTruthy(), {
      interval: 10,
      timeout: 1000,
    });
    expect(screen.getByTestId('loading-indicator').textContent).toEqual('Loading...');

    await waitFor(() => expect(dashboard_get).toHaveBeenCalled());

    await waitFor(() => expect(screen.queryByTestId('loading-indicator')).not.toBeTruthy(), {
      interval: 10,
      timeout: 1000,
    });
    await waitFor(() => expect(screen.getByTestId('dashboard-container')).toBeVisible());
    expect(screen.getByTestId('dashboard-container').childElementCount).toEqual(0);
  });
});
