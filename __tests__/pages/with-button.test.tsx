import { fireEvent, render, screen, waitFor } from '@testing-library/react';

import WithButton from '@/pages/with-button';

import * as Service from '@/services/WhateverApi';
import { asyncData } from '__mocks__/_util';

describe('with button', () => {
  it('renders without error', () => {
    render(<WithButton />);
  });

  it('should respond to interaction', () => {
    render(<WithButton />);

    expect(screen.getByTestId('trigger-button')).toBeVisible();
    expect(screen.getByTestId('data-display')).toBeVisible();

    expect(screen.getByTestId('data-display').textContent).toEqual('no data');

    fireEvent.click(screen.getByTestId('trigger-button'));
    expect(screen.getByTestId('data-display').textContent).toEqual('dog');

    fireEvent.click(screen.getByTestId('trigger-button'));
    expect(screen.getByTestId('data-display').textContent).toEqual('cat');

    fireEvent.click(screen.getByTestId('trigger-button'));
    expect(screen.getByTestId('data-display').textContent).toEqual('hamster');

    fireEvent.click(screen.getByTestId('trigger-button'));
    expect(screen.getByTestId('data-display').textContent).toEqual('no data');
  });

  it('should handle success', async () => {
    let done: boolean = false;

    const api = jest.spyOn(Service, 'API').mockImplementation((data) => {
      done = false;
      if (data !== 'custom data') {
        // ceritanya request nya bakal selesai 1 detik
        return asyncData('not custom data', 1000, () => {
          done = true;
        });
      } else {
        // ini juga sama
        return asyncData(data, 1000, () => {
          done = true;
        });
      }
    });

    render(<WithButton />);

    expect(screen.getByTestId('data-display-2').textContent).toEqual('no data');

    // nunggu sampe dipanggil request nya
    await waitFor(() => expect(api).toHaveBeenCalledTimes(1));
    // nunggu request nya selesai (hardcode 2 detik)
    await waitFor(() => expect(screen.getByTestId('data-display-2').textContent).toEqual('custom data'), {
      interval: 10,
      timeout: 2000,
    });

    // ngelakuin aksi
    fireEvent.click(screen.getByTestId('trigger-button'));
    // nunggu lagi
    await waitFor(() => expect(api).toHaveBeenCalledTimes(2));
    await waitFor(() => expect(screen.getByTestId('data-display-2').textContent).toEqual('not custom data'), {
      interval: 10,
      timeout: 2000,
    });
  });
});
